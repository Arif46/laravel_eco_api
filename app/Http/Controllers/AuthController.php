<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string',
            'email'=>'required',
            'password'=>'required',
        ]);  

        $userregister= new User();
        $userregister->name=$request->name;
        $userregister->email=$request->email;
        $userregister->password=bcrypt($request->password);
        $userregister->save();
        return response()->json(['success'=>'true','message'=>'Register Sucessfully added'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'required|string',
            'password' => 'required'
        ]);
        if(!auth()->attempt($loginData)) {    
            return response()->json(['success'=>false,'user'=>auth()->user(),'token'=>null,
            'message'=>'Sorry your email  or password wrong']);
         }
         $accessToken =auth()->user()->createToken('authToken')->accessToken;
         return response()->json(['success'=>true,'user' =>auth()->user(), 'token' => $accessToken]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
           'status_code'=>200,
            'message'=>'Token Deleted Sucessfully!'
       ]);
    }
}
