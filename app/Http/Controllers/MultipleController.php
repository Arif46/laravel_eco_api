<?php

namespace App\Http\Controllers;

use App\Multiple;
use Image;
use Validator;
use Illuminate\Http\Request;

class MultipleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Multiple::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // $this->validate($request, [
            //    'first_file'=>'required|string',
            //    'secound_file'=>'required|string',
            //    'third_file'=>'required|string',
            // ]); 

            $create  = new Multiple();;
            if ( $request->hasFile( 'first_file' ) ) {
                $image = $request->file( 'first_file' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/first_file/' . $filename ) );
                $create->first_file = 'multiple/first_file/'.$filename;
            }
            if ( $request->hasFile( 'secound_file' ) ) {
                $image = $request->file( 'secound_file' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/secound_file/' . $filename ) );
                $create->secound_file = 'multiple/secound_file/'.$filename;
            } 
            if ( $request->hasFile( 'third_file' ) ) {
                $image = $request->file( 'third_file' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/third_file/' . $filename ) );
                $create->third_file = 'multiple/third_file/'.$filename;
            } 
            $create->save();
            return response()->json(['success'=>'true','message'=>'Upload sucessfully'],201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Multiple  $multiple
     * @return \Illuminate\Http\Response
     */
    public function show(Multiple $multiple)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Multiple  $multiple
     * @return \Illuminate\Http\Response
     */
    public function edit(Multiple $multiple)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Multiple  $multiple
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Multiple $multiple)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Multiple  $multiple
     * @return \Illuminate\Http\Response
     */
    public function destroy(Multiple $multiple)
    {
        //
    }
}
