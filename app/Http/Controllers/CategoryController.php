<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::get();
        // return response()->json([
        //     'success'=>'true',
        //      'category'=>$category
        // ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'name'=>'required|string',
             'description'=>'required',
         ]);
 
         $CategoryCreate= new Category();
         $CategoryCreate->name=$request->name;
         $CategoryCreate->description=$request->description;
         $CategoryCreate->save();

         return response()->json([
             'success'=>'true',
              'message'=>'Category Successfully Uploaded'
         ],201); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $category=Category::where('id',$id)->first();
        return response()->json($category,200); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    // public function edit(Category $category)
    // {
    //     return response()->json($category,200); 
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {  
        $this->validate($request, [
            'name'=>'required|string',
            'description'=>'required',
        ]);

        $category->update($request->all());
    //   $category= Category::where('id', $request->id)->update($request->all());
       return response()->json([
          'sucess'=>true,
           'data' => $category,
      ]);  
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $categorydelete=Category::find($id);
      $categorydelete->delete();
      return response()->json([
           'success'=>'true',
            'message'=>'category Sucessfully delete'
      ],200);
    }

    /**
     * staus change
     */
    public function updatestatuchange($id, $status)
    { 

        // $Updatestatus=::find($id);
        // if($Updatestatus->status){
        //     $Updatestatus->status = 0;
        // }else{
        //     $Updatestatus->status = 1;
        // }
       $change          = Category::find($id); 
       $change->status  = (int)$status;
       $change->update();
       $cartegory = Category::get();
       return response()->json([
           'category' => $cartegory,
           'success'  => 'Status change successfully.'
        ], 201);
    }
}
