<?php

namespace App\Http\Controllers;

use Validator;
use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $subcategories = SubCategory::join('categories','categories.id','sub_categories.category_id')
        //                         ->select('categories.name as category_name','sub_categories.*')
        //                         ->get();  
                                
        $subcategories = SubCategory::with(['category' => function($query) {
            return $query->select(['id','name']);
        }])->get();
                                
        return response()->json([
            'success' => 'true',
            'data'    => $subcategories,
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $this->validate($request, [
            'category_id'=>'required|exists:categories,id',
            'subcategory_name'=>'required|string',
            'description'=>'required',
         ]);
         $subcategoryadd= new SubCategory();
         $subcategoryadd->category_id= $request->category_id;
         $subcategoryadd->subcategory_name= $request->subcategory_name;
         $subcategoryadd->description= $request->description;
         if ($subcategoryadd->save()) {
           return response()->json(['success'=>'true','message'=>'SubCategory Upload Sucessfully'],201);
         } else {
           return response()->json(['message'=>'something went wrong'],403);  
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subcategory=SubCategory::where('id',$id)->first();
        // dd($subCategory);
        return response()->json([
            'data'=>$subcategory
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subcategory)
    {
        $this->validate($request, [
            'category_id'=>'required',
            'subcategory_name'=>'required|string',
            'description'=>'required',
         ]);

        $subcategory->update($request->all());
        return response([
            'message' => 'Update successfully',
            'data'=>$subcategory
        ], 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategorydelete=SubCategory::find($id)->delete();
        return response()->json([
            'success'=>'true',
            'message'=>'Sucessfully delete'
        ],200);
        
    }
}
