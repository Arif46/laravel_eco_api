<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
 

    protected $table = 'sub_categories';

    protected $fillable= [
        'category_id','subcategory_name','description', 'status'
    ];

    // protected $primary_key = 'id';

    public function category(){
        return $this->belongsTo(Category::class);
    }

}
